﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

class MsgWrapper {
	public string msgType;
	public Object data;

	//public string gameId;
	//public int gameTick;

	public MsgWrapper(string msgType, Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public T DeserializeData<T>()
	{
		return JsonConvert.DeserializeObject<T>(this.data.ToString());
	}
}

class YourCarData
{
	public string name = "";
	public string color = "";

}
class PiecePositionData
{
	public int pieceIndex = 0;
	public double inPieceDistance = 0;
	public LanePieceData lane;
}

class LanePieceData
{
	public int startLaneIndex = 0;
	public int endLaneIndex = 0;
}

class IDData
{
	public string name = "";
	//public string key;
	public string color = "";
}

class CarPosition
{
	public IDData id;
	public double angle = 0;
	public PiecePositionData piecePosition;
	public int lap = 0;
}
class DimensionsData
{
	public double length = 0;
	public double width = 0 ;
	public double guideFlagPosition = 0;
}


class CarData
{
	public IDData id;
	public DimensionsData dimensions;
}

class TrackData
{
	public string id = "";
	public string name = "";
	public List<PieceData> pieces;
	public List<LaneData> lanes;
	public List<CarData> cars;
	public StartingPointData startingPoint;

}

class RaceSessionData
{
	public int laps = 0;
	public int maxLapTimeMs = 0;
	public bool quickRace;
}

class GameInitData
{
	public RaceData race;
}

class RaceData
{
	public TrackData track;
	public RaceSessionData raceSession;
}

class LaneData
{
	public int distanceFromCenter = 0;
	public int index = 0;
}

class PositionData
{
	public double x = 0;
	public double y = 0;
}

class StartingPointData
{
	public PositionData position;
	public double angle = 0;
}

class PieceData
{
	public double length = 0;
	public bool @switch = false;
	public int radius = 0;
	public double angle = 0;
	//public int index = 0;
}

class TimeData
{
	public int lap = 0;
	public int ticks = 0;
	public int millis = 0;
}

class RankingData
{
	public int overall = 0;
	public int fastestLap = 0;
}

class LapFinishedData
{
	public IDData car;
	public TimeData lapTime;
	public TimeData raceTime;
	public RankingData ranking;
}
//class CarPositions
//{
//	public List<CarPosition> carPoistions;

//}
abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
		return this;
	}

	protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class SwitchLane : SendMsg
{

    public string value;

    public SwitchLane(string value)
    {
        this.value = value;
    }

	protected override Object MsgData() {
		return this.value;
	}

    protected override string MsgType()
    {
		return "switchLane";
    }
}

class CreateRace : SendMsg
{
    public string trackName;
    public int carCount;
    public BotId botId;

    public CreateRace(string trackName, int carCount, BotId botId)
    {
        this.trackName = trackName;
        this.carCount = carCount;
        this.botId = botId;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }
}

class BotId
{
    public string name;
    public string key;

    public BotId(string name, string key)
    {
        this.name = name;
        this.key = key;
    }
}


enum CarSpeedStateType{
	Accelerate,
	Decelerate,
	Coast
}

enum TurnDangerType{
	Straight,
	SafeTurn,
	DangerTurn,
	CrashTurn
}

