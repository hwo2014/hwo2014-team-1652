#define DEBUG

using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



    public class Bot
    {
        public static void Main(string[] args)
        {

#if(DEBUG)
args = new string[4];

//args[0] = "testserver.helloworldopen.com";
args[0] = "senna.helloworldopen.com";
args[1] = "8091";
args[2] = "UltraSheen";
args[3] = "h+Eq72GtLLo19w";

#endif

            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (TcpClient client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);
                writer.AutoFlush = true;       

#if(DEBUG)

                    var racename = "";
                    Console.WriteLine("Please Choose a race...");
                    Console.WriteLine("0. Default");
                    Console.WriteLine("1. keimola");
                    Console.WriteLine("2. germany");
                    Console.WriteLine("3. usa");
                    Console.WriteLine("4. france");
                    Console.WriteLine("5. ELÄINTARHA");
                    Console.WriteLine("6. ITALY");
                    Console.WriteLine("7. ENGLAND");
                    Console.WriteLine("8. JAPAN");
                    var readkeyinfo = Console.ReadKey();

                    switch (readkeyinfo.Key)
                    {
                        case ConsoleKey.NumPad0:
                        case ConsoleKey.D0:
                            racename = "";
                            break;

                        case ConsoleKey.NumPad1:
                        case ConsoleKey.D1 :
                            racename = "keimola";
                            break;
                        case ConsoleKey.NumPad2:
                        case ConsoleKey.D2:
                            racename = "germany";
                            break;
                        case ConsoleKey.NumPad3:
                        case ConsoleKey.D3:
                            racename = "usa";
                            break;
                        case ConsoleKey.NumPad4:
                        case ConsoleKey.D4:
                            racename = "france";
                            break;
                        case ConsoleKey.NumPad5:
                        case ConsoleKey.D5:
                            racename = "elaeintarha";
                            break;
                        case ConsoleKey.NumPad6:
                        case ConsoleKey.D6:
                            racename = "imola";
                            break;
                        case ConsoleKey.D7:
                        case ConsoleKey.NumPad7:
                            racename = "england";
                            break;
                        case ConsoleKey.NumPad8:
                        case ConsoleKey.D8:
                            racename = "suzuka";
                            break;

                    };
                if(racename != "")
                    new Bot(reader, writer, new Join(racename, 1, new BotId(botName, botKey)));
                else
                    new Bot(reader, writer, new Join(botName, botKey));
#else
                new Bot(reader, writer, new Join(botName, botKey));
#endif

            }
        }




        private StreamWriter writer;
        private YourCarData car;

        private GameInitData racedata;
        private int lastpieceindex = 0;
        private LapFinishedData lapdata;
        private int nextpieceindex = 0;
        private double currentThrottle = 0.9;
        private int gametick = 0;
        private int lastgametick = 0;


        private int pieces = 0;
        private int lastposition = 0;
        private int lastindex = 0;
        private PiecePositionData lasposition;

        private TimeSpan splittime = new TimeSpan();
        private DateTime piecestarttime = new DateTime();

        private int trackplaceindex = 0;
        private double Speed = 0;
        private int currentplaceindex = 0;
        private PieceData thispiece;
        private double safeAngle = 40;
        private int piecestraveled = 0;
        private double totaldistance = 0;
        private bool isLeft = true;

        private int dIntopiece = 0;
        private int pDIntopiece = 0;
        private double curCarAngle = 0;
        private double prevPieceAngle = 0;
        private int lastSwitchedIndex = 0;
        private int fortyFiveCount = 0;
        private double myPrevCarAngle = 0;
        private bool isTurboAvailable = false;
        private bool switchedthispiece = false;
        private List<PieceData> nextturn;
        private string switchtolane = "";
        private bool shouldswitch = false;
        private bool switchdecided = false;
        private bool usingturbo = false;
        private int DistanceToNextTurn;
        private double targetSpeed = 10.0;
        private int tracecounter = 0;

        private CarSpeedStateType carspeedstate = CarSpeedStateType.Accelerate;


        Bot(StreamReader reader, StreamWriter writer, Join join)
        {
       //     Bot(StreamReader reader, StreamWriter writer, CreateRace join)    {
            this.writer = writer;
            string line;

            send(join);

            //GameInitData racedata = null;
            //int lastpieceindex = 0;
            //LapFinishedData lapdata = null;
            //int nextpieceindex = 0;
            //double currentThrottle = 0.9;
            //int gametick = 0;


            //int pieces = 0;
            //int lastposition = 0;
            //int lastindex = 0;

            //TimeSpan splittime = new TimeSpan ();
            //DateTime piecestarttime = new DateTime ();
            ////var lasttick = 0;
            ////PiecePositionData lastdistance = new PiecePositionData();
            //int trackplaceindex = 0;
            //double Speed = 0;
            //var currentplaceindex = 0;
            //PieceData thispiece = new PieceData ();

            thispiece = new PieceData();

            while ((line = reader.ReadLine()) != null)
            {
                //Console.WriteLine("Got a message!");
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                //gametick = msg.gameTick;
                switch (msg.msgType)
                {
                    case "carPositions":
                        GetGameTickFromMessage(line);
                        

                        var positions = msg.DeserializeData<List<CarPosition>>();
                        var ourCar = positions.Where(x => x.id.name == car.name).FirstOrDefault();
                        var opponents = positions.Where(x => x.id.name != car.name).ToList();
                        double newThrottle = currentThrottle;

                     
                        SetPerfectWorldThrottle(ourCar);
                       
                        //AdjustForNearbyRacers(ourCar, opponents);
                        
                        var trot = currentThrottle;
                        if (trot > 1)
                            trot = 1;
                        //send(new Throttle(currentThrottle, gametick));
                        //gametick++;
                        //Console.WriteLine(trot.ToString());
                        switch (carspeedstate)
                        {
                            case CarSpeedStateType.Accelerate:
                                send(new Throttle(trot, gametick));
                                break;
                            case CarSpeedStateType.Decelerate:
                                send(new Throttle(trot, gametick));
                                break;
                            case CarSpeedStateType.Coast:
                                //send(new Throttle(trot, gametick));
                                break;
                            case CarSpeedStateType.Switch:
                                Console.WriteLine("Switched to " + switchtolane + " lane");
                                switchedthispiece = true;
                                send(new SwitchLane(switchtolane, gametick));
                                lastSwitchedIndex = nextpieceindex;
                                //switchdecided = false;
                                break;
                            case CarSpeedStateType.Turbo:
                                isTurboAvailable = false;
                                send(new Turbo("Burn!!!", gametick));
                                Console.WriteLine("Burn Baby Burn!!!, Turbo used");
                                carspeedstate = CarSpeedStateType.Coast;
                                //currentThrottle = 0.1;
                                break;

                        }



                        break;
                    case "lapFinished":
                        GetGameTickFromMessage(line);
                        lapdata = msg.DeserializeData<LapFinishedData>();
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("lapFinished, Lap: " + lapdata.lapTime.lap.ToString() + ",laptime: " + (lapdata.lapTime.millis).ToString() + ", racetime: " + (lapdata.raceTime.millis).ToString());
                        Console.ResetColor();
                        send(new Ping());
                        break;
                    case "join":
                        //GetGameTickFromMessage(line);
                        Console.WriteLine("Joined");
                        send(new Ping());
                        break;

                    case "joinRace":
                        Console.WriteLine("Joined");
                        send(new Ping());

                        break;
                    case "createRace":
                        Console.WriteLine("Race Created");
                        send(new Ping());

                        break;

                    case "gameInit":
                        racedata = msg.DeserializeData<GameInitData>();

                        Console.WriteLine("Race init, Track: " + racedata.race.track.name);
                        //Console.WriteLine("Race init");
                        send(new Ping());
                        break;

                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        send(new Ping());
                        break;

                    case "yourCar":
                        //YourCarData car = msg.DeserializeData<YourCarData>();
                        car = msg.DeserializeData<YourCarData>();
                        Console.WriteLine("Your Car: " + car.name + ", " + "color: " + car.color);
                        send(new Ping());
                        break;

                    case "gameStart":
                        GetGameTickFromMessage(line);
                        piecestarttime = DateTime.Now;
                        Console.WriteLine("Race starts");
                        //send(new Throttle(1));
                        send(new Ping());
                        break;

                    case "turboAvailable" :
                        GetGameTickFromMessage(line);
                        isTurboAvailable = true;

                        Console.WriteLine("turboAvailable");

                        break;
                    case "turboStart":
                        GetGameTickFromMessage(line);
                        usingturbo = true;
                        isTurboAvailable = false;
                        Console.WriteLine("turboStart");

                        break;
                    case "turboEnd":
                        GetGameTickFromMessage(line);
                        usingturbo = false;
                        Console.WriteLine("turboEnd");

                        break;

                    case "dnf":
                        Console.WriteLine("dnf");
                        send(new Ping());
                        break;

                    case "finish":
                        Console.WriteLine("finish");
                        send(new Ping());
                        break;

                    case "crash":
                        GetGameTickFromMessage(line);
                        Console.WriteLine(string.Format("CRASH REPORT Throttle: {0}", currentThrottle));
                        Console.WriteLine(string.Format("CRASH REPORT Speed: {0}/{1}", Speed, targetSpeed));
                        Console.WriteLine(string.Format("CRASH REPORT Piece Angle: {0}", thispiece.angle));
                        Console.WriteLine(string.Format("CRASH REPORT Prev Piece Angle: {0}", prevPieceAngle));

                        Console.WriteLine(string.Format("CRASH REPORT Prev InPiece: {0}", pDIntopiece));
                        Console.WriteLine(string.Format("CRASH REPORT InPiece: {0}", dIntopiece));
                        Console.WriteLine(string.Format("CRASH REPORT Car Angle: {0}", curCarAngle));
                        Console.WriteLine(string.Format("CRASH REPORT 45: {0}", fortyFiveCount));
                        Console.WriteLine(string.Format("CRASH REPORT Prev Car Angle: {0}", myPrevCarAngle));

                        Console.WriteLine("crash");
                        usingturbo = false;
                        send(new Ping());
                        break;

                    case "spawn":
                        GetGameTickFromMessage(line);
                        Console.WriteLine("spawn");
                        targetSpeed = 20;
                        currentThrottle = 0.1;
                        send(new Throttle(currentThrottle));
                        break;

                    case "tournamentEnd":
                        Console.WriteLine("tournamentEnd");
                        send(new Ping());
                        break;

                    default:
                        Console.WriteLine(msg.msgType + " not handled, " + msg.data.ToString());
                        send(new Ping());
                        break;
                }
                //gametick++;
                lastgametick = gametick;
            }
            Console.WriteLine("Done!");
            Console.ReadLine();
        }

        private void UpdateSpeed(CarPosition ourCar)
        {
            var distance = 0.0;
            if (lasposition == null)
            {
                lasposition = ourCar.piecePosition;
                Speed = 0;
                return;
            }

            if (lasposition.pieceIndex != ourCar.piecePosition.pieceIndex)
            {
                var lasttrackpiece = racedata.race.track.pieces[lasposition.pieceIndex];

                distance = (CalcLength(lasttrackpiece) - lasposition.inPieceDistance) + ourCar.piecePosition.inPieceDistance;
            }
            else
            {
                distance = ourCar.piecePosition.inPieceDistance - lasposition.inPieceDistance;
            }

            //totaldistance = (piecestraveled * 100) + ourCar.piecePosition.inPieceDistance;
            //currentplaceindex = totaldistance;
            if (gametick > 0)
                Speed = distance * 10;
            trackplaceindex = currentplaceindex;
            lasposition = ourCar.piecePosition;
        }

        private void GetGameTickFromMessage(string line)
        {
            MsgWrapperWithGameTick msgtick = JsonConvert.DeserializeObject<MsgWrapperWithGameTick>(line);
            gametick = msgtick.gameTick;
        }

        private void AdjustForNearbyRacers(CarPosition ourCar, List<CarPosition> opponents)
        {
            return;
        }

        private void AssessNextTurn(CarPosition ourCar)
        {

            thispiece = racedata.race.track.pieces[ourCar.piecePosition.pieceIndex];
            nextturn = GetNextTurn(ourCar.piecePosition.pieceIndex);
            var turnanglesum = 0.0;
                
                foreach(var t in nextturn)
                {
                    turnanglesum += Math.Abs(t.angle); 
                }
            Console.WriteLine("Next turn: " + turnanglesum.ToString());

        }

        private List<PieceData> GetNextTurn(int currentpieceindex)
        {
            var returnlist = new List<PieceData>();
            var currentpiece = thispiece;
            var startindex = currentpieceindex;
            PieceData turnstart = new PieceData();
            var angle = 0;
            var foundturn = false;

            //if (currentpieceindex == racedata.race.track.pieces.Count - 1)
                //currentpieceindex = 0;

            for (var x = currentpieceindex; x <= racedata.race.track.pieces.Count; x++)
            {
                DistanceToNextTurn += 100;
                if (x == racedata.race.track.pieces.Count)
                    x = 0;

                var next = racedata.race.track.pieces[x];
                if (Math.Abs(next.angle) > 0)
                {
                    foundturn = true;
                    returnlist.Add(next);
                }
                if (foundturn && Math.Abs(next.angle) == 0)
                    break;

               
            }
            return returnlist;
        }

        private void SetPerfectWorldThrottle(CarPosition ourCar)
        {

            if (ourCar == null)
                return;
           


            prevPieceAngle = thispiece.angle;

            thispiece = racedata.race.track.pieces[ourCar.piecePosition.pieceIndex];
            //thispiece.index = ourCar.piecePosition.pieceIndex;
            nextpieceindex = ourCar.piecePosition.pieceIndex + 1;
            var _2awayIndex = nextpieceindex + 1;
            var _3awayIndex = _2awayIndex + 1;

            var checkForSwitch = false;

            //AssessNextTurn(ourCar);


            if (ourCar.piecePosition.pieceIndex != lastpieceindex)
            {
                piecestraveled++;
                checkForSwitch = true;
                //switchedthispiece = false;

                //thispiece = racedata.race.track.pieces[lastpieceindex];



                if (thispiece.angle > 0 && !thispiece.@switch)
                {
                    //                  0123456789012345678901234567890123456789
                    Console.WriteLine("Bend  (" + ourCar.piecePosition.lane.startLaneIndex.ToString() + ") angle: " + thispiece.angle.ToString() + ",length: " + Math.Truncate(CalcLength(thispiece)).ToString() +
                        ",(" + gametick.ToString() + ")throttle: " + Math.Truncate(currentThrottle * 100).ToString() + ",angle: " + Math.Truncate(ourCar.angle).ToString() +
                        ",Speed: " + Math.Truncate(Speed).ToString() + "/" + Math.Truncate(targetSpeed).ToString());


                    //if (Math.Abs(thispiece.angle) == 45)
                    //{
                    //    if (Math.Abs(prevPieceAngle) == 45)
                    //        fortyFiveCount += 1;
                    //    else
                    //        fortyFiveCount = 1;
                    //}
                    switchdecided = false;
                }else
                if (thispiece.@switch)
                {
                    //Console.WriteLine ("Car can switch,angle: " + thispiece.angle.ToString () + ",throttle: " + currentThrottle.ToString () + ",angle: " + Math.Truncate (ourCar.angle).ToString () + ",Speed: " + Math.Truncate (Speed).ToString ());
                    Console.WriteLine("switch(" + ourCar.piecePosition.lane.startLaneIndex.ToString() + "),(" + gametick.ToString() + ")throttle: " + Math.Truncate(currentThrottle * 100).ToString()
                        + " angle: " + Math.Truncate(ourCar.angle).ToString() + ",Speed: " + Math.Truncate(Speed).ToString() + "/" + Math.Truncate(targetSpeed).ToString());

                    //if (!switchedthispiece) {

                    //}
                }else
                if (thispiece.angle == 0)
                {
                    Console.WriteLine("strate(" + ourCar.piecePosition.lane.startLaneIndex.ToString() + "),(" + gametick.ToString() + ")throttle: " + Math.Truncate(currentThrottle * 100).ToString() 
                        + ",angle: " + Math.Truncate(ourCar.angle).ToString() + ",Speed: " + Math.Truncate(Speed).ToString() + "/" + Math.Truncate(targetSpeed).ToString());
                    //AssessNextTurn(ourCar);
                    switchdecided = false;
                }

                lastpieceindex = ourCar.piecePosition.pieceIndex;

            }

            UpdateSpeed(ourCar);
            
           

            var prevCarAngle = curCarAngle;
            myPrevCarAngle = prevCarAngle;

            var CarAngle = Math.Abs(ourCar.angle);
            var thisPieceAngle = Math.Abs(thispiece.angle);



            pDIntopiece = dIntopiece;

            int distanceIntoThisPiece = (int)ourCar.piecePosition.inPieceDistance;
            dIntopiece = distanceIntoThisPiece;
            curCarAngle = ourCar.angle;

            if (nextpieceindex == racedata.race.track.pieces.Count)
            {
                nextpieceindex = 0;
                _2awayIndex = 1;
                _3awayIndex = 2;
            }
            else if (nextpieceindex + 1 == racedata.race.track.pieces.Count)
            {
                _2awayIndex = 0;
                _3awayIndex = 1;
            }
            else if (nextpieceindex + 2 == racedata.race.track.pieces.Count)
            {
                _3awayIndex = 0;
            }


            //	if (nextpieceindex < racedata.race.track.pieces.Count) 
            //  {
            var nextpiece = racedata.race.track.pieces[nextpieceindex];
            var nextPieceAngle = Math.Abs(nextpiece.angle);
            var _2AwayPieceAngle = racedata.race.track.pieces[_2awayIndex].angle;
            var _2AwayPiece = racedata.race.track.pieces[_2awayIndex];
            var _3AwayPieceAngle = racedata.race.track.pieces[_3awayIndex].angle;
            var _3AwayPiece = racedata.race.track.pieces[_3awayIndex];

            //var safeSpeed = 7;
            // var safeAngle = 45;
            //var maxSpeed = 10;

            //if (racedata.race.track.name.ToLower() == "keimola" || racedata.race.track.name.ToLower() == "usa")
                //Keimola(ourCar.angle, Speed, nextPieceAngle > 22.5 ? true : false, thisPieceAngle > 22.5 ? true : false, distanceIntoThisPiece, prevCarAngle, _2AwayPieceAngle > 22.5 ? true : false);
        //    else if (racedata.race.track.name.ToLower() == "germany")
         //       Germany(ourCar.angle, Speed, nextPieceAngle > 22.5 ? true : false, thisPieceAngle > 22.5 ? true : false, distanceIntoThisPiece, prevCarAngle, _2AwayPieceAngle > 22.5 ? true : false, nextPieceAngle == 22.5 ? true : false);
            //else
                //RaceLogic(ourCar.angle, prevCarAngle, thispiece, nextpiece, _2AwayPiece,_3AwayPiece, distanceIntoThisPiece);
            AlternateRaceLogic(ourCar.angle, prevCarAngle, thispiece, nextpiece, _2AwayPiece, _3AwayPiece, distanceIntoThisPiece);

                //Keimola(ourCar.angle, Speed, nextPieceAngle > 22.5 ? true : false, thisPieceAngle > 22.5 ? true : false, distanceIntoThisPiece, prevCarAngle, _2AwayPieceAngle > 22.5 ? true : false);



            if (nextpiece.@switch && !switchdecided && !usingturbo && CarAngle < 45)
            {
                //DetermineIfShouldSwitch(ourCar);
            }

            if (isTurboAvailable && nextpiece.angle == 0 && _2AwayPieceAngle == 0 && _3AwayPieceAngle == 0 && !usingturbo)
            {
                //carspeedstate = CarSpeedStateType.Turbo;
            }




            //if (checkForSwitch)
            //{
            //    var switchtolane = "";
            //    //var nextnextpiece = nextpieceindex + 1;
            //    var shouldswitch = false;
            //    //var nextAfterSwitchPiece = racedata.race.track.pieces[nextnextpiece];

            //    var currentlaneindex = ourCar.piecePosition.lane.startLaneIndex;
            //    //Console.WriteLine ("current Lane: " + ourCar.piecePosition.lane.startLaneIndex.ToString ());
            //    var currentlane = racedata.race.track.lanes[currentlaneindex];
            //    //Console.WriteLine ("currentlane.distanceFromCenter: " + currentlane.distanceFromCenter);
            //    var _2awayPiece = racedata.race.track.pieces[_2awayIndex];

            //    if (nextpiece.@switch)
            //    {
            //        switch (currentlaneindex)
            //        {
            //            case 0:
            //                if (_2AwayPieceAngle == 45)
            //                {
            //                    switchtolane = "Right";
            //                    shouldswitch = true;
            //                }
            //                break;
            //            case 1:
            //                if (_2AwayPieceAngle == 45)
            //                {
            //                    switchtolane = "Left";
            //                    shouldswitch = true;
            //                }
            //                break;
            //        }
            //        //Console.WriteLine ("Inside Lane: " + switchtolane + ", Current Lane: " + (currentlaneindex == 0 ? "Left" : "Right") + "D: " + ourCar.piecePosition.inPieceDistance.ToString ());
            //        if (shouldswitch)
            //        {
            //            Console.WriteLine("Switched to " + switchtolane + " lane");
            //            switchedthispiece = true;
            //            send(new SwitchLane(switchtolane));
            //            lastSwitchedIndex = nextpieceindex;
            //        }
            //    }
            //}
















            //}

            piecestarttime = DateTime.Now;
           

        }

        private void DetermineIfShouldSwitch(CarPosition ourCar)
        {
            if (racedata.race.track.pieces[nextpieceindex].@switch && lastSwitchedIndex != nextpieceindex)
            {
                var nextnextpiece = nextpieceindex + 1;
                //var shouldswitch = false;
                var nextAfterSwitchPiece = new PieceData();
               
                var nextanglepieceindex = nextnextpiece;
                var trynextlap = false;
                if (nextanglepieceindex < racedata.race.track.pieces.Count)
                {
                    if (nextanglepieceindex < racedata.race.track.pieces.Count)
                        nextAfterSwitchPiece = racedata.race.track.pieces[nextanglepieceindex];
                    else
                        nextAfterSwitchPiece = racedata.race.track.pieces[0];


                    while (Math.Abs(nextAfterSwitchPiece.angle) != 45)
                    {
                        nextanglepieceindex++;
                        if (nextanglepieceindex < racedata.race.track.pieces.Count)
                            nextAfterSwitchPiece = racedata.race.track.pieces[nextanglepieceindex];
                        else
                        {
                            trynextlap = true;
                            break;
                        }
                        //Console.WriteLine ("finding next piece" + Math.Abs (nextAfterSwitchPiece.angle).ToString() + ", current nextanglepieceindex: " + nextanglepieceindex.ToString() + ", angle: " + nextAfterSwitchPiece.angle.ToString());

                    }
                    //Console.WriteLine ("next angle piece angle: " + nextAfterSwitchPiece.angle.ToString ());
                }
                if (trynextlap)
                {
                    nextanglepieceindex = 0;
                    nextAfterSwitchPiece = racedata.race.track.pieces[nextanglepieceindex];

                    while (Math.Abs(nextAfterSwitchPiece.angle) != 45)
                    {
                        nextanglepieceindex++;
                        if (nextanglepieceindex < racedata.race.track.pieces.Count)
                            nextAfterSwitchPiece = racedata.race.track.pieces[nextanglepieceindex];
                        else
                            break;
                        //Console.WriteLine ("finding next piece" + Math.Abs (nextAfterSwitchPiece.angle).ToString() + ", current nextanglepieceindex: " + nextanglepieceindex.ToString() + ", angle: " + nextAfterSwitchPiece.angle.ToString());

                    }
                    trynextlap = false;
                }

                var currentlaneindex = ourCar.piecePosition.lane.startLaneIndex;
                //Console.WriteLine ("current Lane: " + ourCar.piecePosition.lane.startLaneIndex.ToString ());
                var currentlane = racedata.race.track.lanes[currentlaneindex];
                //Console.WriteLine ("currentlane.distanceFromCenter: " + currentlane.distanceFromCenter);
                switch (currentlaneindex)
                {
                    case 0:
                        if (Math.Abs(nextAfterSwitchPiece.angle) == 45)
                        {
                            switchtolane = "Right";
                            shouldswitch = true;
                        }
                        break;
                    case 1:
                        if (Math.Abs(nextAfterSwitchPiece.angle) == 45)
                        {
                            switchtolane = "Left";
                            shouldswitch = true;
                        }
                        break;
                }
                //Console.WriteLine ("Inside Lane: " + switchtolane + ", Current Lane: " + (currentlaneindex == 0 ? "Left" : "Right") + "D: " + ourCar.piecePosition.inPieceDistance.ToString ());
                if (shouldswitch)
                {
                    carspeedstate = CarSpeedStateType.Switch;
                    shouldswitch = false;
                    //Console.WriteLine("Switched to " + switchtolane + " lane");
                    //switchedthispiece = true;
                    ////send(new SwitchLane(switchtolane));
                    //lastSwitchedIndex = nextpieceindex;
                }
                switchdecided = true;
            }
        }

        private double getAngularSpeed(PiecePositionData currentposition, PiecePositionData lastposition, int currenttick, int lasttick)
        {
            var angS = (2 * Math.PI) / (currenttick - lasttick);

            return angS;
        }

        private double getSpeed(PiecePositionData currentposition, PiecePositionData lastposition, int currenttick, int lasttick)
        {
            var lasttotaldistance = (lastposition.pieceIndex * 100) + lastposition.inPieceDistance;
            var currenttotaldistance = (currentposition.pieceIndex * 100) + currentposition.inPieceDistance;

            var time = currenttick - lasttick;

            return (currenttotaldistance - lasttotaldistance) / time;

        }

        private double CalcLength(PieceData piece)
        {
            if(piece.angle == 0)
                return piece.length;



            return Math.Abs((2 * piece.radius) * Math.PI * (piece.angle / 260));
        }


         private void AlternateRaceLogic(double carAngleR, double prevCarAngleR, PieceData currentPiece, PieceData nextPiece, PieceData _2AwayPiece, PieceData _3AwayPiece, int distanceIntoThisPiece)
        {
            var carAngle = Math.Abs(carAngleR);
            var prevCarAngle = Math.Abs(prevCarAngleR);
            var angChange = ((carAngle - prevCarAngle) / prevCarAngle) * 100;

            var curPAng = Math.Abs(currentPiece.angle);
            var nextPAng = Math.Abs(nextPiece.angle);
            var _2AwayPAng = Math.Abs(_2AwayPiece.angle);
            var _3AwayPAng = Math.Abs(_3AwayPiece.angle);

            var anglesum = curPAng + nextPAng + _2AwayPAng + _3AwayPAng;
            var distancesum = Math.Abs(CalcLength(currentPiece) + CalcLength(nextPiece) + CalcLength(_2AwayPiece) + CalcLength(_3AwayPiece));



            if (distancesum == 0)
            {
            }
           



            if (usingturbo)
            {
                carspeedstate = CarSpeedStateType.Coast;
                currentThrottle = 0;
                return;
            }

            var turn = new TurnDangerType();

            if (anglesum == 180 && distancesum < 400)
            {
                turn = TurnDangerType.ExtremeTurn;
            }
            else if (anglesum == 180 && distancesum >= 400)
            {
                turn = TurnDangerType.CrashTurn;
            }
            else if (anglesum == 157.5 && distancesum < 400)
            {
                turn = TurnDangerType.ExtremeTurn;
            }
            else if (anglesum == 157.5 && distancesum >= 400)
            {
                turn = TurnDangerType.CrashTurn;
            }
            else if (anglesum == 135 && distancesum < 400)
            {
                turn = TurnDangerType.ExtremeTurn;
            }
            else if (anglesum == 135 && distancesum >= 400)
            {
                turn = TurnDangerType.CrashTurn;
            }
            else if ((anglesum < 135 && anglesum > 90) && distancesum < 400)
            {
                turn = TurnDangerType.ExtremeTurn;
            }
            else if ((anglesum < 135 && anglesum > 90) && distancesum >= 400)
            {
                turn = TurnDangerType.SafeTurn;
            }
            else if ((anglesum <= 90  && anglesum > 0) && distancesum < 400)
            {
                turn = TurnDangerType.ExtremeTurn;
            }
            else if ((anglesum <= 90 && anglesum > 0) && distancesum >= 400)
            {
                turn = TurnDangerType.SafeTurn;
            }
            else
                turn = TurnDangerType.Straight;

            if (tracecounter == 9)
            {
                //System.IO.File.AppendAllText(@"C:\dump\angles\" + racedata.race.track.name + ".txt", "AngleSum: " + anglesum.ToString() + ", DistanceSum: " + distancesum.ToString() + "," + turn.ToString() + Environment.NewLine);
                //Console.WriteLine("AngleSum: " + anglesum.ToString() + ", DistanceSum: " + distancesum.ToString() + "," + turn.ToString());
                tracecounter = 0;
            }
            else
                tracecounter++;



            GetSpeedforCarAngle(carAngle, turn, anglesum, distancesum, distanceIntoThisPiece);

            var diff = Math.Abs(targetSpeed - Speed);
            var speedchange = 0.1;
            if (diff > 25)
                speedchange = 1;
            else if (diff > 10 && diff <= 25)
                speedchange = 0.5;
            else if (diff > 1 && diff <= 10)
                speedchange = 0.25;
            else
                speedchange = 0.1;
            if (Speed < targetSpeed)
            {
                //if (diff > 25)
                //    speedchange = 1;
                //else if (diff > 10 && diff <= 25)
                //    speedchange = 0.5;
                //else if (diff > 1 && diff <= 10)
                //    speedchange = 0.25;
                //else
                //    speedchange = 0.1;

                //if (carAngle > 40)
                //    speedchange = 1.0;

                //if (carAngle > 20 && carAngle < 40)
                //    speedchange = 0.5;

                //if (carAngle > 6 && carAngle < 20)
                //    speedchange = 0.25;

                SpeedUp(speedchange);
            }
            else if(Speed > targetSpeed)
             {
                 
                 //if (carAngle > 40)
                 //    speedchange = 1.0;

                 //if (carAngle > 20 && carAngle < 40)
                 //    speedchange = 0.5;

                 //if (carAngle > 6 && carAngle < 20)
                 //    speedchange = 0.25;

                /SlowDown(speedchange);
             }//SlowDown
            else if (Speed == targetSpeed)
            {
                //currentThrottle = 0;
            }
  
        }

         private void GetSpeedforCarAngle(double carAngle, TurnDangerType turntype, double anglesum, double distancesum, double distanceIntoThisPiece)
         {
             var safeCarAngle = 24;

             //if (distancesum == 0)
             //    distancesum = 1;
             //var dangerweight = (distancesum - distanceIntoThisPiece) / (anglesum + (carAngle * 2));
             //if (distancesum > 500)
             //{
             //    Console.WriteLine(distancesum.ToString());
             //}

             //Console.WriteLine("danger factor: " + Math.Truncate(dangerweight).ToString());

             //targetSpeed = dangerweight * 20;

             //currentThrottle = Clamp(dangerweight * 0.21,0.0,1.0);

             switch (turntype)
             {
                 case TurnDangerType.Straight:
                     if (carAngle < 12)
                         targetSpeed = 100;
                     else
                         targetSpeed = 90;
                     break;
                 case TurnDangerType.SafeTurn:
                     if (carAngle < safeCarAngle)
                         targetSpeed = 90;
                     else
                         targetSpeed = 80;
                     break;
                 case TurnDangerType.DangerTurn:
                     if (carAngle < safeCarAngle)
                         targetSpeed = 70;
                     else
                         targetSpeed = 60;
                     break;
                 case TurnDangerType.CrashTurn:
                     if (carAngle < safeCarAngle)
                         targetSpeed = 60;
                     else
                         targetSpeed = 55;
                     break;
                 case TurnDangerType.ExtremeTurn:
                     if (carAngle < safeCarAngle)
                         targetSpeed = 50;
                     else
                         targetSpeed = 45;
                     break;

             }
             //return 0.0;
         }



        private void RaceLogic(double carAngleR, double prevCarAngleR, PieceData currentPiece, PieceData nextPiece, PieceData _2AwayPiece, PieceData _3AwayPiece, int distanceIntoThisPiece)
        {
            var carAngle = Math.Abs(carAngleR);
            var prevCarAngle = Math.Abs(prevCarAngleR);
            var angChange = ((carAngle - prevCarAngle) / prevCarAngle) * 100;

            var curPAng = Math.Abs(currentPiece.angle);
            var nextPAng = Math.Abs(nextPiece.angle);
            var _2AwayPAng = Math.Abs(_2AwayPiece.angle);


            if (usingturbo)
            {
                carspeedstate = CarSpeedStateType.Coast;
                currentThrottle = 0;
                return;
            }

            if (curPAng == 0 && nextPAng == 0 && _2AwayPAng != 45)
                //if(carAngle > 40)
                //    SlowDown(0.1);
                //else
                if(isTurboAvailable)
                {
                    carspeedstate = CarSpeedStateType.Turbo;
                }
                else
                    SpeedUp(0.3);//SpeedUpQuck();
            else if(curPAng == 0 && nextPAng == 0 && _2AwayPAng == 45)
            {
                if(Speed >= 6)
                {
                    SlowDown(0.2);
                }
                else
                {
                    SpeedUp(0.1);
                }
            }
            else if(curPAng == 0 && nextPAng == 45)
            {
                if(Speed >= 8)
                    SlowDown(1.0);
                else if (Speed >= 4 && Speed < 8)
                {
                    SlowDown(0.5);
                }
                else
                {
                    SpeedUp(0.1);
                    //SlowDown(0.2);
                }
               
            }
            else if (curPAng == 0 && nextPAng == 22.5)
            {
                //if (distanceIntoThisPiece > 50)
                //    if(Speed >= 6)
                //        SlowDown(0.1);
                //else
                    SpeedUp(0.1);
            }  
            else if(curPAng == 22.5 && nextPAng == 0)
            {
    
                //    if(prevCarAngle <carAngle && Speed >= 6)
                //        SlowDown(0.1, 0.0);
                //else
                if (carAngle < 30)
                    SpeedUp(0.3);
                else
                    SlowDown(0.1);
             
            }
            else if(curPAng == 22.5 && nextPAng == 22.5)
            {
                //if (carAngle > 30)
                //    SlowDown(0.5, 0.0);
                //else
                //{
                    if (prevCarAngle < carAngle & Speed >= 7)
                        SlowDown(0.1, 0.0);
                    else
                        SpeedUp(0.2);
             //   }

            }
            else if(curPAng == 22.5 && nextPAng == 45)
            {
                SlowDown(0.1);
            }
            else if (curPAng == 45 && nextPAng == 0)
            {
                if (carAngle > 30 || prevCarAngle < carAngle)
                    SlowDown(0.5, 0.0);//SlowDown(0.5, 0.0);
                else
                    SpeedUp(0.5);
            }
            else if (curPAng == 45 && nextPAng == 22.5)
            {
                if (carAngle > 20 || prevCarAngle < carAngle || angChange > 10)
                    SlowDown(0.2, 0.0);//SlowDown(0.5, 0.0);
                else
                    SpeedUp(0.1);
            }
            else if (curPAng == 45 && nextPAng == 45)
            {
                //if ((prevCarAngle < carAngle && carAngle > 10) || angChange > 20 )
                //    SlowDown(0.1, 0.0);
                //else
                //    SpeedUp(0.1);





                if (prevCarAngle < carAngle || angChange > 10)
                {
                    if (carAngle > 30 || Speed >= 7)
                        SlowDown(0.4);
                    else
                        SlowDown(0.1);
                }
                else
                    SpeedUp(0.1); //SpeedUpQuck();
            }






            //if (nextPiece.angle > 0)
            //    if (Speed >= 7)
            //        SlowDown(0.8);
            //    else if (prevCarAngle < carAngle)
            //    {
            //        SlowDown(0.4, 0.1);
            //    }
            //    else
            //        SpeedUp(0.1, 0.1, 4.0);
            //else
            //{
            //    if (prevCarAngle < carAngle)
            //    {
            //        SlowDown(0.4, 0.1);
            //    }
            //    else
            //        SpeedUpQuck();
            //}

        }






        private void Keimola(double carAngleR, double currentSpeed, bool nextIs45Angle, bool currentIs45Angle, int distanceIntoPiece, double prevCarAngleR, bool _2AwayIs45Angle)
        {
            var carAngle = Math.Abs(carAngleR);
            var prevCarAngle = Math.Abs(prevCarAngleR);
            var angChange = ((carAngle - prevCarAngle) / prevCarAngle) * 100;
            //  var abCarAngle = Math.Abs(carAngle);

            if (currentIs45Angle && nextIs45Angle && _2AwayIs45Angle)
            {
                //   Console.WriteLine(string.Format("cur ang: {0} | prev angle: {1}", carAngleR, prevCarAngleR));
                // if ((angChange > 5 && carAngleR > 0) || (angChange < 5 && carAngleR < 0))
                if (prevCarAngle < carAngle)
                {
                    if (carAngle > 30 || Speed >= 7)
                        SlowDown(0.4);
                    else
                        SlowDown(0.1);
                    //else if (distanceIntoPiece < 55)
                    //   SlowDown(0.3);
                    //else
                    //    SpeedUp(0.3);
                }
                else
                    SpeedUpQuck();//SpeedUp();//_45SpeedUp();
            }
            else if (currentIs45Angle && nextIs45Angle && !_2AwayIs45Angle)
            {
                //   Console.WriteLine(string.Format("cur ang: {0} | prev angle: {1}", carAngleR, prevCarAngleR));
                //if ((angChange > 5 && carAngleR > 0) || (angChange < 5 && carAngleR < 0))
                if (prevCarAngle < carAngle)
                {
                    if (carAngle > 30 || Speed >= 7)
                        SlowDown(0.3);
                    else
                        SlowDown(0.1);
                }
                else
                    SpeedUpQuck();//SpeedUp();//_45SpeedUp();
            }
            else if (currentIs45Angle && !nextIs45Angle)
            {
                if (carAngle < 55)
                    SpeedUpQuck();
                else
                {
                    if (distanceIntoPiece > 61)
                        SpeedUpQuck();
                    else
                        SpeedUp(0.5);

                }
            }
            else if (!currentIs45Angle && nextIs45Angle && _2AwayIs45Angle)
            {
                if (Speed == 10)
                    SlowDown(0.5);
                else if (Speed == 9)
                    SlowDown(0.4);
                else if (Speed == 8)
                    SlowDown(0.3);
                else
                    SlowDown(0.1);
            }
            else if (!currentIs45Angle && nextIs45Angle && !_2AwayIs45Angle) //Sharp Turn
            {
                if (Speed == 10)
                    SlowDown(0.8);
                else if (Speed == 9)
                    SlowDown(0.7);
                else if (Speed == 8)
                    SlowDown(0.6);
                else
                    SlowDown(0.5);
            }
            else if (!currentIs45Angle && !nextIs45Angle)
                SpeedUpQuck();






        }



        private void Germany(double carAngleR, double currentSpeed, bool nextIs45Angle, bool currentIs45Angle, int distanceIntoPiece, double prevCarAngleR, bool _2AwayIs45Angle, bool nextIs22Angle)
        {
            var carAngle = Math.Abs(carAngleR);
            var prevCarAngle = Math.Abs(prevCarAngleR);
            var angChange = ((carAngle - prevCarAngle) / prevCarAngle) * 100;
            //  var abCarAngle = Math.Abs(carAngle);

            if (currentIs45Angle && nextIs45Angle && _2AwayIs45Angle)
            {
                if (prevCarAngle < carAngle)
                {
                    SlowDown(0.9);
                }
                else
                    SpeedUp(0.3, 0.0, 0.9);//_45SpeedUp();
            }
            else if (currentIs45Angle && nextIs45Angle && !_2AwayIs45Angle)
            {
                if (prevCarAngle < carAngle)
                {
                    SlowDown(0.9);
                }
                else
                    SpeedUp(0.4, 0.0, 0.9);//SpeedUp();//_45SpeedUp();
            }
            else if (currentIs45Angle && !nextIs45Angle)
            {
                if (carAngle < 40)
                    SpeedUp(0.4, 0.0, 0.9);
                else
                    SlowDown(0.8);
            }
            else if (!currentIs45Angle && nextIs45Angle)
            {
                if (Speed >= 7)
                    SlowDown(0.9);
                else
                {
                    if (distanceIntoPiece > 40)
                        SlowDown(0.5);
                    else
                        SlowDown(0.1);
                }
            }

            else if (!currentIs45Angle && !nextIs45Angle)
            {
                if (nextIs22Angle)
                {
                    if (Speed >= 7)
                        SlowDown(0.2);
                    else if (Speed > 4 && Speed < 7)
                        SlowDown(0.1);
                    else
                        SpeedUp(0.9, 0.0, 0.9);
                }
                else
                {

                    if (prevCarAngle < carAngle)
                    {
                        SlowDown(0.9);
                    }
                    else
                        SpeedUp(0.9, 0.0, 0.9);//_45SpeedUp();

                }
            }
            else
            {
                if (prevCarAngle < carAngle)
                {
                    SlowDown(0.1);
                }
            }
        }



        private void StuffAndThings(double CarAngle, double currentSpeed, bool nextIs45Angle, bool currentIs45Angle, int distanceIntoPiece)
        {
            if (CarAngle >= 0 && CarAngle < 10)
            {
                if (currentIs45Angle && nextIs45Angle)
                    if (Speed >= 6)
                        _45SpeedUp();
                    else
                        SpeedUp();
                else if (currentIs45Angle && !nextIs45Angle)
                    //if (distanceIntoPiece > 51)
                    SpeedUpQuck();
                //else
                //    SpeedUp();
                else if (!currentIs45Angle && nextIs45Angle)
                    if (distanceIntoPiece > 65)
                        _45SlowDown();
                    else
                        SlowDown();
                else if (!currentIs45Angle && !nextIs45Angle)
                    SpeedUpQuck();

            }
            else if (CarAngle >= 10 && CarAngle < 20)
            {
                if (currentIs45Angle && nextIs45Angle)
                    if (Speed >= 8)
                        _45SlowDown();
                    else if (Speed >= 4)
                        SlowDown();
                    else
                        _45SpeedUp();
                else if (currentIs45Angle && !nextIs45Angle)
                    if (distanceIntoPiece > 51)
                        SpeedUpQuck();
                    else
                        SpeedUp();
                else if (!currentIs45Angle && nextIs45Angle)
                    if (distanceIntoPiece > 60)
                        _45SlowDown();
                    else
                        SlowDown();
                else if (!currentIs45Angle && !nextIs45Angle)
                    SpeedUpQuck();

            }
            else if (CarAngle >= 20 && CarAngle < 30)
            {
                if (currentIs45Angle && nextIs45Angle)
                    if (Speed >= 6)
                        _45SlowDown();
                    else if (Speed >= 4)
                        SlowDown();
                    else
                        _45SpeedUp();
                else if (currentIs45Angle && !nextIs45Angle)
                    if (distanceIntoPiece > 60)
                        SpeedUpQuck();
                    else
                        SpeedUp();
                else if (!currentIs45Angle && nextIs45Angle)
                    if (Speed >= 7)
                        _45SlowDown();
                    else
                        SlowDown();
                else if (!currentIs45Angle && !nextIs45Angle)
                    SpeedUpQuck();
            }
            else if (CarAngle >= 30 && CarAngle < 45)
            {
                if (currentIs45Angle && nextIs45Angle)
                    _45SlowDown();
                else if (currentIs45Angle && !nextIs45Angle)
                    if (distanceIntoPiece > 60)
                        SpeedUp();
                    else
                        _45SlowDown();
                else if (!currentIs45Angle && nextIs45Angle)
                    _45SlowDown();
                else if (!currentIs45Angle && !nextIs45Angle)
                    SpeedUp();
            }
            else
                if (currentIs45Angle || nextIs45Angle)
                    ZeroThrottle();
                else
                    SlowDown();
        }


        private void SafeAngleCheck(double CarAngle)
        {
            if (CarAngle > safeAngle)
                SlowDown();
            else
                SpeedUp();
        }

        private void FloorItBro()
        {
            carspeedstate = CarSpeedStateType.Accelerate;
            currentThrottle = 1.0;
        }

        private void SpeedUpQuck()
        {
            //   Console.WriteLine(string.Format("Speed UP! - throttle: {0}", currentThrottle));
            carspeedstate = CarSpeedStateType.Accelerate;
            //if (currentThrottle + 0.15 <= 1.0)
            currentThrottle += 1;
            currentThrottle = Clamp(currentThrottle, 0.0, 1.0);
            // else
            //    currentThrottle = 1.0;


            //return currentThrottle;
        }

        public double Clamp(double value, double min, double max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }


        private void ZeroThrottle()
        {
            carspeedstate = CarSpeedStateType.Decelerate;
            currentThrottle = 0;
            currentThrottle = Clamp(currentThrottle, 0.0, 1.0);
        }


        private void _45SpeedUp()
        {
            currentThrottle += 0.1;
            currentThrottle = Clamp(currentThrottle, 0.0, 1.0);
        }

        private void _45SlowDown()
        {
            currentThrottle -= 0.3;
            currentThrottle = Clamp(currentThrottle, 0.0, 1.0);
        }

        private void SpeedUp()
        {
            //   Console.WriteLine(string.Format("Speed UP! - throttle: {0}", currentThrottle));

            //if(currentThrottle + 0.111 <= 1.0)

            carspeedstate = CarSpeedStateType.Accelerate;
            currentThrottle += 0.5;
            currentThrottle = Clamp(currentThrottle, 0.0, 1.0);
            // else
            //     currentThrottle = 1.0;

            //return currentThrottle;
        }

        private void SlowDown(double value, double min = 0.0, double max = 1.0)
        {
            carspeedstate = CarSpeedStateType.Decelerate;
            currentThrottle -= value;
            currentThrottle = Clamp(currentThrottle, min, max);
        }




        private void SpeedUp(double value, double min = 0.0, double max = 1.0)
        {
            carspeedstate = CarSpeedStateType.Accelerate;
            currentThrottle += value;
            currentThrottle = Clamp(currentThrottle, min, max);
        }

        private void SlowDown()
        {
            //  Console.WriteLine(string.Format("Slow DOWN! - throttle: {0}", currentThrottle));
            carspeedstate = CarSpeedStateType.Decelerate;
            //if (currentThrottle - 0.09 >= 0.0)
            currentThrottle -= 0.1;
            currentThrottle = Clamp(currentThrottle, 0.0, 1.0);



            //return currentThrottle;
        }

        private void send(SendMsg msg)
        {
            writer.WriteLine(msg.ToJson());
        }
    }


    #region models

    class MsgWrapper
    {
        public string msgType;
        public Object data;

        //public string gameId;
        //public int gameTick;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }

        public T DeserializeData<T>()
        {
            return JsonConvert.DeserializeObject<T>(this.data.ToString());
        }
    }

    class MsgWrapperWithGameTick
    {
        public string msgType;
        public Object data;

        public string gameId;
        public int gameTick;

        public MsgWrapperWithGameTick(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }

        public T DeserializeData<T>()
        {
            return JsonConvert.DeserializeObject<T>(this.data.ToString());
        }
    }

    class YourCarData
    {
        public string name = "";
        public string color = "";

    }
    class PiecePositionData
    {
        public int pieceIndex = 0;
        public double inPieceDistance = 0;
        public LanePieceData lane;
    }

    class LanePieceData
    {
        public int startLaneIndex = 0;
        public int endLaneIndex = 0;
    }

    class IDData
    {
        public string name = "";
        //public string key;
        public string color = "";
    }

    class CarPosition
    {
        public IDData id;
        public double angle = 0;
        public PiecePositionData piecePosition;
        public int lap = 0;
    }
    class DimensionsData
    {
        public double length = 0;
        public double width = 0;
        public double guideFlagPosition = 0;
    }


    class CarData
    {
        public IDData id;
        public DimensionsData dimensions;
    }

    class TrackData
    {
        public string id = "";
        public string name = "";
        public List<PieceData> pieces;
        public List<LaneData> lanes;
        public List<CarData> cars;
        public StartingPointData startingPoint;

    }

    class RaceSessionData
    {
        public int laps = 0;
        public int maxLapTimeMs = 0;
        public bool quickRace;
    }

    class GameInitData
    {
        public RaceData race;
    }

    class RaceData
    {
        public TrackData track;
        public RaceSessionData raceSession;
    }

    class LaneData
    {
        public int distanceFromCenter = 0;
        public int index = 0;
    }

    class PositionData
    {
        public double x = 0;
        public double y = 0;
    }

    class StartingPointData
    {
        public PositionData position;
        public double angle = 0;
    }

    class PieceData
    {
        public double length = 0;
        public bool @switch = false;
        public int radius = 0;
        public double angle = 0;
        //public int index = 0;
    }

    class TimeData
    {
        public int lap = 0;
        public int ticks = 0;
        public int millis = 0;
    }

    class RankingData
    {
        public int overall = 0;
        public int fastestLap = 0;
    }

    class LapFinishedData
    {
        public IDData car;
        public TimeData lapTime;
        public TimeData raceTime;
        public RankingData ranking;
    }
    //class CarPositions
    //{
    //	public List<CarPosition> carPoistions;

    //}
    abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }

    class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;
        public string msgType;

        public string trackName;
        public int carCount;
        public BotId botId;
        public string password = "";

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            //this.color = "red";
            msgType = "join";
        }

        public Join(string trackName, int carCount, BotId botId)
        {
            this.trackName = trackName;
            this.carCount = carCount;
            this.botId = botId;

            msgType = "createRace";
        }

        protected override string MsgType()
        {
            return msgType;
        }
    }

    class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }

    class Throttle : SendMsg
    {
        public double value;
        public int gametick;

        public Throttle(double value)
        {
            this.value = value;
        }

         public Throttle(double value , int gameTick)
        {
            this.value = value;
             this.gametick = gameTick;
        }

         protected int gameTick()
         {
             return this.gametick;
         }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }

    class SwitchLane : SendMsg
    {

        public string value;
        public int gametick;

        public SwitchLane(string value)
        {
            this.value = value;
        }
        public SwitchLane(string value, int gameTick)
        {
            this.value = value;
            this.gametick = gameTick;
        }

        protected int gameTick()
        {
            return this.gametick;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }

    class Turbo : SendMsg
    {

        public string message;
        public int gametick;

        public Turbo(string message)
        {
            this.message = message;
        }

        public Turbo(string message , int gametick)
        {
            this.message = message;
            this.gametick = gametick;
        }

        protected int gameTick()
        {
            return this.gametick;
        }

        protected override Object MsgData()
        {
            return this.message;
        }

       protected override string MsgType()
        {
            return "turbo";
        }
    }


    class CreateRace : SendMsg
    {
        public string trackName;
        public int carCount;
        public BotId botId;

        public CreateRace(string trackName, int carCount, BotId botId)
        {
            this.trackName = trackName;
            this.carCount = carCount;
            this.botId = botId;
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }

    class BotId
    {
        public string name;
        public string key;

        public BotId(string name, string key)
        {
            this.name = name;
            this.key = key;
        }
    }


    enum CarSpeedStateType
    {
        Accelerate,
        Decelerate,
        Coast,
        Switch,
        Turbo
    }

    enum TurnDangerType
    {
        Straight,
        SafeTurn,
        DangerTurn,
        CrashTurn,
        ExtremeTurn
    }

    #endregion
